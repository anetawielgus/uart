#include "servo.h"

#define DETECTOR_bm 1<<10

enum eServoState {CALLIB, IDLE, IN_PROGRESS};

struct Servo{
enum eServoState eState;
unsigned int uiCurrentPosition;
unsigned int uiDesiredPosition;
};

struct Servo sServo;
enum DetectorState{INACTIVE,ACTIVE};

enum DetectorState eReadDetector(){
	if((IO0PIN & DETECTOR_bm) == 0){
		return ACTIVE;
	}
	else{
		return INACTIVE;
	}	
}

void Automat (void){

	switch(sServo.eState){
		
				case CALLIB:
					if(eReadDetector() == ACTIVE){
						sServo.uiCurrentPosition = 0;
						sServo.uiDesiredPosition = 0;
						sServo.eState = IDLE;
					}
					else  {
						Led_StepLeft();
						sServo.eState = CALLIB;
					}
				break;
		
				case IDLE:
					if(sServo.uiDesiredPosition == sServo.uiCurrentPosition){
						sServo.eState = IDLE;
					}
					else  {
						sServo.eState = IN_PROGRESS;
					}
				break;
					
				case IN_PROGRESS:
					if(sServo.uiCurrentPosition > sServo.uiDesiredPosition){
						Led_StepLeft();
						sServo.uiCurrentPosition--;
						sServo.eState = IN_PROGRESS;
					}
					else if(sServo.uiCurrentPosition < sServo.uiDesiredPosition){
						Led_StepRight();
						sServo.uiCurrentPosition++;
						sServo.eState = IN_PROGRESS;
					}
					else {
						sServo.eState = IDLE;
					}
				break;
		}
	}
	
void DetectorInit(void){
	IO0DIR = IO0DIR & (~(DETECTOR_bm));
}
void ServoCallib(void){
		sServo.eState = CALLIB;
}
void ServoInit(unsigned int uiServoFrequency){
	Led_Init();
	DetectorInit();
	ServoCallib();
	Timer1Interrupts_Init((1000000/uiServoFrequency), &Automat);
	
}
void ServoGoTo(unsigned int uiPosition){
	sServo.uiDesiredPosition = uiPosition;
}
