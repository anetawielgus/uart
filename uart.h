#define TERMINATOR 0x0d


enum eRecieverStatus {EMPTY, READY, OVERFLOW};
enum eTransmiterStatus {FREE, BUSY};

void UART_InitWithInt(unsigned int uiBaudRate);
void RecieverInit(void);
void Reciever_PutCharacterToBuffer(char cCharacter);
enum eRecieverStatus eReciever_GetStatus(void);
void Reciever_GetStringCopy(char * ucDestination);
void Transmiter_SendString(char cString[]);
char Transmiter_GetCharacterFromBuffer(void);
enum eTransmiterStatus Transmiter_GetStatus(void);
