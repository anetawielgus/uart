#include "keyboard.h"
#include "servo.h"
#include "uart.h"
#include "decode.h"

extern struct Watch sWatch;
char cMessageTransimtterd[16];
char cMessageRecieved[16];
extern unsigned char ucTokenNr;
extern struct Token asToken[MAX_TOKEN_NR];
unsigned char ucfSendCal;
unsigned char ucfSendId;
unsigned char ucfSendUnknownCommand;
unsigned char ucfSendCallib;
unsigned char ucfSendGoTo;

int main (){
	
	Timer0Interrupts_Init(1000000, &WatchUpdate);
	ServoInit(100);
	UART_InitWithInt(9600);
	Transmiter_SendString(cMessageTransimtterd);
	
	while(1){
		
		if(eReciever_GetStatus() == READY){
				Reciever_GetStringCopy(cMessageRecieved);
				DecodeMsg(cMessageRecieved);
				if(ucTokenNr > 0 && asToken[0].eType == KEYWORD){
					switch(asToken[0].uValue.eKeyword){
						case CAL: 
							ucfSendCal = 1;
							break;
						case ID:
							ucfSendId = 1;
							break;
						case CALLIB:
							ucfSendCallib = 1;
							break;
						case GOTO:
							ucfSendGoTo = 1;
							break;
						default:
							ucfSendUnknownCommand = 1;
							break;
					}
				}
				else{
					ucfSendUnknownCommand = 1;
				}
		}
		
		if(Transmiter_GetStatus() == FREE){
			if(sWatch.fMinutesValueChanged == 1){
				sWatch.fMinutesValueChanged = 0;
				CopyString("min ", cMessageTransimtterd);
				AppendUIntToString(sWatch.ucMinutes, cMessageTransimtterd );
				AppendString("\n" , cMessageTransimtterd);
				Transmiter_SendString(cMessageTransimtterd);
			}
			else if(sWatch.fSeccondsValueChanged == 1){
				sWatch.fSeccondsValueChanged=0;
				CopyString("sec ", cMessageTransimtterd);
				AppendUIntToString( sWatch.ucSecconds, cMessageTransimtterd );
				AppendString("\n" , cMessageTransimtterd);
				Transmiter_SendString(cMessageTransimtterd);
			}
			else if(ucfSendCal == 1){
				ucfSendCal = 0;
				CopyString("calc " , cMessageTransimtterd);
				AppendUIntToString(2*asToken[1].uValue.uiNumber , cMessageTransimtterd);
				AppendString("\n" , cMessageTransimtterd);
				Transmiter_SendString(cMessageTransimtterd);
				}
			else if (ucfSendId == 1){
				ucfSendId = 0;
				CopyString("id arm servo" , cMessageTransimtterd);
				AppendString("\n" , cMessageTransimtterd);
				Transmiter_SendString(cMessageTransimtterd);
			}
			else if(ucfSendUnknownCommand == 1){
				ucfSendUnknownCommand = 0;
				CopyString("unkonowncommand" , cMessageTransimtterd);
				AppendString("\n" , cMessageTransimtterd);
				Transmiter_SendString(cMessageTransimtterd);
			}
			else if(ucfSendCallib == 1){
				ucfSendCallib = 0;
				ServoCallib();
				CopyString("callib ok ", cMessageTransimtterd);
				AppendString("\n" , cMessageTransimtterd);
				Transmiter_SendString(cMessageTransimtterd);
			}
			else if(ucfSendGoTo == 1){
				ucfSendGoTo = 0;
				ServoGoTo(asToken[1].uValue.uiNumber);
				CopyString("goto ok ", cMessageTransimtterd);
				AppendString("\n" , cMessageTransimtterd);
				Transmiter_SendString(cMessageTransimtterd);
			}
		}
	}
}
