#include "string.h"

#define MAX_TOKEN_NR 3
#define MAX_KEYWORD_STRING_LTH 14
#define MAX_KEYWORD_NR 4
#define MAX_TOKEN_NR 3
#define RECIEVER_SIZE 12

enum TokenType {KEYWORD, NUMBER, STRING};
enum KeywordCode { CAL, ID, CALLIB, GOTO};
enum StanAutomatu {TOKEN, DELIMITER};

union TokenValue {
	enum KeywordCode eKeyword;
	unsigned int uiNumber;
	char *pcString;
};

struct Keyword {
	enum KeywordCode eCode;
	char cString[MAX_KEYWORD_STRING_LTH + 1];
};

struct Token {
	enum TokenType eType;
	union TokenValue uValue;
};


unsigned char ucFindTokensInString (char *pcString);
enum ReturnValue eStringToKeyword (char pcStr[],enum KeywordCode *peKeywordCode);
void DecodeTokens(void);
void DecodeMsg(char *pcString);
