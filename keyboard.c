#include <LPC21xx.H>
#include "keyboard.h"

#define BUTT1_bm 0x000000010
#define BUTT2_bm 0x000000020
#define BUTT3_bm 0x000000040
#define BUTT4_bm 0x000000080 
 
void KeyboardInit (){
		  IO0DIR = IO0DIR & (~(BUTT1_bm | BUTT2_bm | BUTT3_bm | BUTT4_bm ));
	}
		
		enum eButtonState eKeyboard_Read(){
				if ((IO0PIN & BUTT1_bm) == 0) {
				  return BUTTON_1;
				}
				else if ((IO0PIN & BUTT2_bm) == 0) {
					return BUTTON_2;
			  }
				else if ((IO0PIN & BUTT3_bm) == 0){
					return BUTTON_3;
				}	
				else if ((IO0PIN & BUTT4_bm)== 0){
					return BUTTON_4;
				}
				else {
					return RELASED;
				}
		}
