#include <LPC21xx.H>

#include "led.h"
#include "timer_interrupts.h"

void Automat (void);
void DetectorInit(void);
void ServoCallib(void);
void ServoInit(unsigned int uiServoFrequency);
void ServoGoTo(unsigned int uiPosition);
