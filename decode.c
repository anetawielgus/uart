#include "decode.h"

#define NULL 0
#define MAX_KEYWORD_STRING_LTH 14
#define MAX_KEYWORD_NR 4

unsigned char ucTokenNr;

struct Token asToken[MAX_TOKEN_NR];

struct Keyword asKeywordList[MAX_KEYWORD_NR]= {
	{CAL,"calc"},
	{ID, "id" },
	{CALLIB, "callib"},
	{GOTO, "goto"}
};

unsigned char ucFindTokensInString (char *pcString){
	enum StanAutomatu eAktualnyStan = DELIMITER;
	unsigned char ucAktualnyZnak, ucLicznikZnakow;
	ucTokenNr = 0;
	for(ucLicznikZnakow = 0; ; ucLicznikZnakow++){
		ucAktualnyZnak = pcString[ucLicznikZnakow];
		if((ucTokenNr == MAX_TOKEN_NR) || (NULL == ucAktualnyZnak)){
			return ucTokenNr;
		}
		switch(eAktualnyStan){
			case TOKEN:
				if(ucAktualnyZnak == ' '){
					eAktualnyStan = DELIMITER;
				}
				else{
					eAktualnyStan = TOKEN;
				}
			break;
				
			case DELIMITER:
				if(ucAktualnyZnak != ' '){
					eAktualnyStan = TOKEN;
					asToken[ucTokenNr].uValue.pcString = &pcString[ucLicznikZnakow];
					ucTokenNr++;
				}
				else{
					eAktualnyStan = DELIMITER;
				}
				break;
		}
	}
}


enum ReturnValue eStringToKeyword (char pcStr[],enum KeywordCode *peKeywordCode){
	unsigned char ucLicznikZnakow;
	for(ucLicznikZnakow = 0; ucLicznikZnakow < MAX_KEYWORD_NR; ucLicznikZnakow++){
		if(EQUAL == eCompareString(pcStr,asKeywordList[ucLicznikZnakow].cString )){
		*peKeywordCode = asKeywordList[ucLicznikZnakow].eCode;
		return OK;
		}
	}
	return ERROR;
}
void DecodeTokens(void){
	unsigned char ucLicznikTokenow;
	struct Token *sAktualnyToken;
	union TokenValue uCurrentValue;
	for(ucLicznikTokenow = 0; ucLicznikTokenow < ucTokenNr; ucLicznikTokenow++){
		sAktualnyToken = &asToken[ucLicznikTokenow];
		if(OK == eStringToKeyword(sAktualnyToken -> uValue.pcString, &uCurrentValue.eKeyword)){
			sAktualnyToken->eType = KEYWORD;
			sAktualnyToken->uValue = uCurrentValue;
		}
		else if(OK == eHexStringToUInt(sAktualnyToken->uValue.pcString, &uCurrentValue.uiNumber)){
			sAktualnyToken->eType = NUMBER;
			sAktualnyToken->uValue = uCurrentValue;
		}
		else{
			sAktualnyToken->eType = STRING;
		}
	}
}
void DecodeMsg(char *pcString){
	ucFindTokensInString(pcString);
	ReplaceCharactersInString(pcString, ' ', NULL);
	DecodeTokens();
}
