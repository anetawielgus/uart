#include <LPC21xx.H>
#include "timer.h"

#define COUNTER_ENABLE_bm 1<<0
#define COUNTER_RESET_bm 1<<1
#define INTERRUPT_ON_MR0_bm 1<<0
#define RESET_ON_MR0_bm 1<<1
#define MR0_IINTERRUPT_bm 1<<0

void InitTimer0(void){
	T0TCR = COUNTER_ENABLE_bm;
}

void WaitOnTimer0(unsigned int uiTime){
	T0TCR = T0TCR | COUNTER_ENABLE_bm;
	T0TCR = T0TCR | COUNTER_RESET_bm;
	T0TCR = T0TCR  & ~(COUNTER_RESET_bm);
	uiTime = 15000 * uiTime;
	while(T0TC < uiTime) {
	}
}

void InitTimer0Match0(unsigned int iDelayTime){
	T0MCR = RESET_ON_MR0_bm;
	T0MCR = T0MCR | INTERRUPT_ON_MR0_bm;
	T0TCR = T0TCR | COUNTER_ENABLE_bm;
	T0TCR = T0TCR | COUNTER_RESET_bm;
	T0TCR = T0TCR  & ~(COUNTER_RESET_bm);
	T0MR0 = 15000*iDelayTime;
}

void WaitOnTimer0Match0(void){
	while(T0IR != MR0_IINTERRUPT_bm){
	}
	T0IR = MR0_IINTERRUPT_bm;
}
