#include <LPC210X.H>
#include "uart.h"
#include "string.h"

/************ UART ************/
//PINSEL0 PIN Control Register
#define UART0_TxD_bm 0x00000001
#define UART0_RxD_bm 0x00000004
// U0LCR Line Control Register
#define mDIVISOR_LATCH_ACCES_BIT                   0x00000080
#define m8BIT_UART_WORD_LENGTH                     0x00000003

// UxIER Interrupt Enable Register
#define mRX_DATA_AVALIABLE_INTERRUPT_ENABLE        0x00000001
#define mTHRE_INTERRUPT_ENABLE                     0x00000002

// UxIIR Pending Interrupt Identification Register
#define mINTERRUPT_PENDING_IDETIFICATION_BITFIELD  0x0000000F
#define mTHRE_INTERRUPT_PENDING                    0x00000002
#define mRX_DATA_AVALIABLE_INTERRUPT_PENDING       0x00000004

/************ Interrupts **********/
// VIC (Vector Interrupt Controller) channels
#define VIC_UART0_CHANNEL_NR  6
#define VIC_UART1_CHANNEL_NR  7

// VICVectCntlx Vector Control Registers
#define mIRQ_SLOT_ENABLE                           0x00000020

//**********Strings Receive**********/
#define RECIEVER_SIZE 16
#define TRANSMITER_SIZE 16
#define NULL 0

////////////// Zmienne globalne ////////////
char cOdebranyZnak;
unsigned char ucASCIICharacter;

//*****Transmiter*****///
struct TransmiterBuffer{
	char cData[TRANSMITER_SIZE];
	enum eTransmiterStatus eStatus;
	unsigned char fLastCharacter;
	unsigned char cCharCtr;
};

struct TransmiterBuffer sTransmiterBuffer;

char Transmiter_GetCharacterFromBuffer(){
	char cReturnedValue;
	
	if(sTransmiterBuffer.fLastCharacter == 0){
		sTransmiterBuffer.eStatus = BUSY;
		switch(sTransmiterBuffer.cData[sTransmiterBuffer.cCharCtr]){
	
			case NULL:
				cReturnedValue = TERMINATOR;
				sTransmiterBuffer.fLastCharacter = 1;
			break;
			
			default:
				cReturnedValue = sTransmiterBuffer.cData[sTransmiterBuffer.cCharCtr];
				sTransmiterBuffer.fLastCharacter = 0;
			break;
		}
	}
	
	else{
		cReturnedValue = NULL;
		sTransmiterBuffer.eStatus = FREE;
	}
	sTransmiterBuffer.cCharCtr++;
	return cReturnedValue;
	
}

void Transmiter_SendString(char cString[]){
	CopyString(cString,sTransmiterBuffer.cData);
	sTransmiterBuffer.cCharCtr = 0;
	sTransmiterBuffer.fLastCharacter = 0;
	sTransmiterBuffer.eStatus = BUSY;
	U0THR = sTransmiterBuffer.cData[sTransmiterBuffer.cCharCtr];
	sTransmiterBuffer.cCharCtr ++;
}

enum eTransmiterStatus Transmiter_GetStatus(void){
	return sTransmiterBuffer.eStatus;
}

//*****Receiver********//
struct RecieverBuffer{
	char cData[RECIEVER_SIZE];
	unsigned char ucCharCtr;
	enum eRecieverStatus eStatus;
};

struct RecieverBuffer sRecieverBuffer;

void RecieverInit(){
	sRecieverBuffer.ucCharCtr = 0;
}

void Reciever_PutCharacterToBuffer(char cCharacter){
	
	switch(cCharacter){
		
		case TERMINATOR :
			cCharacter=NULL;
			sRecieverBuffer.cData[sRecieverBuffer.ucCharCtr] = cCharacter;
			if(sRecieverBuffer.ucCharCtr < RECIEVER_SIZE){
				sRecieverBuffer.eStatus = READY;
			}
			else{
				sRecieverBuffer.eStatus = OVERFLOW;
			}
			sRecieverBuffer.ucCharCtr = 0;
			break;
		
		default:
			if(sRecieverBuffer.ucCharCtr >= (RECIEVER_SIZE - 1)){
				sRecieverBuffer.eStatus = OVERFLOW;
			}
			else{
				sRecieverBuffer.cData[sRecieverBuffer.ucCharCtr] = cCharacter;
				sRecieverBuffer.ucCharCtr++;
				sRecieverBuffer.eStatus = EMPTY;
			}
			break;
	}
}

enum eRecieverStatus eReciever_GetStatus(void){
	return sRecieverBuffer.eStatus;
}

void Reciever_GetStringCopy(char * ucDestination){
	
	unsigned char ucCounter;
	for(ucCounter = 0; sRecieverBuffer.cData[ucCounter]; ++ucCounter) {
		ucDestination[ucCounter] = sRecieverBuffer.cData[ucCounter];
	}
	ucDestination[ucCounter] = sRecieverBuffer.cData[ucCounter];
	
	sRecieverBuffer.eStatus = EMPTY;
}

///////////////////////////////////////////
__irq void UART0_Interrupt (void) {
   // jesli przerwanie z odbiornika (Rx)
   
   unsigned int uiCopyOfU0IIR=U0IIR; // odczyt U0IIR powoduje jego kasowanie wiec lepiej pracowac na kopii

   if      ((uiCopyOfU0IIR & mINTERRUPT_PENDING_IDETIFICATION_BITFIELD) == mRX_DATA_AVALIABLE_INTERRUPT_PENDING) // odebrano znak
   {
      Reciever_PutCharacterToBuffer(U0RBR); //odebranie znaku do lancucha znakowego
   } 
   
   if ((uiCopyOfU0IIR & mINTERRUPT_PENDING_IDETIFICATION_BITFIELD) == mTHRE_INTERRUPT_PENDING)              // wyslano znak - nadajnik pusty 
   {
		if(sTransmiterBuffer.eStatus == BUSY){
			U0THR=Transmiter_GetCharacterFromBuffer();
		}
		else{
		}
   }

   VICVectAddr = 0; // Acknowledge Interrupt
}

////////////////////////////////////////////
void UART_InitWithInt(unsigned int uiBaudRate){

   // UART0
   PINSEL0 = PINSEL0 | UART0_TxD_bm | UART0_RxD_bm;             // ustawic pina na odbiornik uart0
   U0LCR  |= m8BIT_UART_WORD_LENGTH | mDIVISOR_LATCH_ACCES_BIT; // dlugosc slowa, DLAB = 1
   U0DLL   = ((15000000)/16)/uiBaudRate;                        // predkosc transmisji
   U0LCR  &= (~mDIVISOR_LATCH_ACCES_BIT);                       // DLAB = 0
   U0IER  |= mRX_DATA_AVALIABLE_INTERRUPT_ENABLE | mTHRE_INTERRUPT_ENABLE;               // ??? co tu robinmy - enables the Receive Data Available interrupt for UART0.

   // INT
   VICVectAddr2  = (unsigned long) UART0_Interrupt;             // set interrupt service routine address
   VICVectCntl2  = mIRQ_SLOT_ENABLE | VIC_UART0_CHANNEL_NR;     // use it for UART 0 Interrupt
   VICIntEnable |= (0x1 << VIC_UART0_CHANNEL_NR);               // Enable UART 0 Interrupt Channel
}

