struct Watch {
	unsigned char ucMinutes, ucSecconds;
	unsigned char fSeccondsValueChanged, fMinutesValueChanged;
};

void Timer0Interrupts_Init(unsigned int uiPeriod, void (*ptrInterruptFunction)());
void Timer1Interrupts_Init(unsigned int uiPeriod, void (*ptrInterruptFunction)());
void WatchUpdate(void);
